# Hacking Reactive Commons

Author: Esteban García Marín

Email: esteban.garcia@sofka.com.co

## Requerimientos Para Ejecutar Proyecto

*Tener instalado o dockerizado rabbit local con un virtual host: test

## Indicaciones

Patrón aplicado: Direct Command

Caso de Uso: Envío de correo electrónico a través de un llamado a un api y luego por comando entre 2 módulos.

### Pasos

1. Ejecutar los dos proyectos sender y receiver con su respectiva clase MainApplication.
2. Realizar un llamado POST al siguiente endpoint: http://127.0.0.1:4004/sendEmail y el siguiente cuerpo:
```json
{
    "dni": "1036648452",
    "name": "Esteban García Marín",
    "email": "esteban.garcia@sofka.com.co"
}
```
Se puede modificar los valores a su gusto.
3. Puede comprobar la ejecución de los proyectos y el envío del comando mediante los logs:

Sender
```java
        log.info("Se inicia la aplicación para enviar correos electrónicos por comando.");
        log.info("Se recibe petición REST y se envía comando para enviar correo electrónico a: ".concat(personaInfo.getEmail()));
```
Receiver
```java
        log.info("Se inicia la aplicación para recibir comandos de correos electrónicos.");
        log.info("Comando recibido para enviar correo electrónico a: ".concat(personaInfoCommand.getData().getEmail()));
```