package co.com.sofka.estebang.handler;

import co.com.sofka.estebang.handler.dto.PersonaInfo;
import lombok.extern.java.Log;
import org.reactivecommons.async.api.HandlerRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

@Log
@Configuration
public class Handler {
    private static final String COMMAND_NAME = "sendEmail";

    @Bean
    public HandlerRegistry commandHandlerRegistry() {
        return HandlerRegistry.register()
                .handleCommand(COMMAND_NAME, personaInfoCommand -> {
                    log.info("Comando recibido para enviar correo electrónico a: ".concat(personaInfoCommand.getData().getEmail()));
                    return Mono.empty();
                }, PersonaInfo.class);
    }
}
