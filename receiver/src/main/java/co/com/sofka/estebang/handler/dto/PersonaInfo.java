package co.com.sofka.estebang.handler.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonaInfo {
    private String dni;
    private String name;
    private String email;
}
