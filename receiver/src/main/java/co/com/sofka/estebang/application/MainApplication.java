package co.com.sofka.estebang.application;

import co.com.sofka.estebang.handler.Handler;
import lombok.extern.java.Log;
import org.reactivecommons.async.impl.config.annotations.EnableMessageListeners;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableMessageListeners
@Log
@ComponentScan(basePackageClasses = Handler.class)
public class MainApplication {
    public static void main(String[] args) {
        log.info("Se inicia la aplicación para recibir comandos de correos electrónicos.");
        SpringApplication.run(MainApplication.class, args);
    }
}
