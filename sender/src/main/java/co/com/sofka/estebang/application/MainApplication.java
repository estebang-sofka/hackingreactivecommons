package co.com.sofka.estebang.application;

import co.com.sofka.estebang.service.Service;
import lombok.extern.java.Log;
import org.reactivecommons.async.impl.config.annotations.EnableDirectAsyncGateway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableDirectAsyncGateway
@SpringBootApplication
@Log
@ComponentScan(basePackageClasses = Service.class)
public class MainApplication {
    public static void main(String[] args) {
        log.info("Se inicia la aplicación para enviar correos electrónicos por comando.");
        SpringApplication.run(MainApplication.class, args);
    }
}
