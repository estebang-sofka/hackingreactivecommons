package co.com.sofka.estebang.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersonaInfo {
    private String dni;
    private String name;
    private String email;
}
