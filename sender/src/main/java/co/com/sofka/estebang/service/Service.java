package co.com.sofka.estebang.service;

import co.com.sofka.estebang.service.dto.PersonaInfo;
import lombok.extern.java.Log;
import org.reactivecommons.api.domain.Command;
import org.reactivecommons.async.api.DirectAsyncGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@Log
public class Service {

    private static final String COMMAND_NAME = "sendEmail";
    private static final String TARGET = "receiver";

    @Autowired
    private DirectAsyncGateway directAsyncGateway;

    @PostMapping(path = "/sendEmail", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Void> sendEmail(@RequestBody PersonaInfo personaInfo) {
        log.info("Se recibe petición REST y se envía comando para enviar correo electrónico a: ".concat(personaInfo.getEmail()));
        return directAsyncGateway.sendCommand(new Command<>(COMMAND_NAME, UUID.randomUUID().toString(), personaInfo),
                TARGET);
    }

}
